﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TunierVerwaltungBusiness;

namespace WinFormsApp1
{
    public partial class SpielerAnlegenForm : Form
    {
        public SpielerAnlegenForm()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            var Vorname = textBox1.Text;
            var Nachname = textBox2.Text;
            var Club = textBox3.Text;
            var HCPI = int.Parse(textBox4.Text);
            var newSpieler = new Spieler(Vorname, Nachname, Club, HCPI );

            SpielerService.SpielerListe.Add(newSpieler);

            this.Close();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
