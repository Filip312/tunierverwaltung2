﻿using System;
using System.Collections.Generic;
using TunierVerwaltungBusiness;

namespace Hello_World
{
    class Program //Klassen
    {
        static void Main(string[] args) //Methode (Funktion)
        {
            int TurnierID = 0;
            int ID = 6;
            List<Spieler> SpielerListe = new List<Spieler>();
            List<Turnier> TurnierListe = new List<Turnier>();
            //Hauptprogramm

            SpielerListe.Add(ProgramMethoden.SpielerObjektErstellen("Jan", "Svacina", "Fischland", 12));
            SpielerListe.Add(ProgramMethoden.SpielerObjektErstellen("Filip", "Svacina", "Fischland", 12));
            SpielerListe.Add(ProgramMethoden.SpielerObjektErstellen("Olli", "Svacina", "Fischland", 12));
            SpielerListe.Add(ProgramMethoden.SpielerObjektErstellen("Bernd", "Svacina", "Fischland", 12));
            SpielerListe.Add(ProgramMethoden.SpielerObjektErstellen("Horst", "Svacina", "Fischland", 12));

            Turnier turnierTmp = TurnierObjektErstellen("Club Meisterschaften", "03.09.2021", ++TurnierID);
            turnierTmp.AddTeilnehmer(SpielerListe[0], 70);
            turnierTmp.AddTeilnehmer(SpielerListe[1], 80);
            turnierTmp.AddTeilnehmer(SpielerListe[2], 90);
            turnierTmp.AddTeilnehmer(SpielerListe[3], 100);
            turnierTmp.AddTeilnehmer(SpielerListe[4], 120);
            TurnierListe.Add(turnierTmp);

            //turnierTmp.AddTeilnehmer(ProgramMethoden.SpielerObjektErstellen("Filip", "Marks", "Fischland", 16, ++ID), 97);
            //turnierTmp.AddTeilnehmer(ProgramMethoden.SpielerObjektErstellen("Patrick", "Marks", "Fischland", 11, ++ID), 88);
            //turnierTmp.AddTeilnehmer(ProgramMethoden.SpielerObjektErstellen("Halvard", "Moebius", "Fischland", 17, ++ID), 84);
            //turnierTmp.AddTeilnehmer(ProgramMethoden.SpielerObjektErstellen("Oli", "Lange", "Fischland", 34, ++ID), 75);
            //turnierTmp.AddTeilnehmer(ProgramMethoden.SpielerObjektErstellen("Paul", "Schults", "Fischland", 45, ++ID), 68);
            //turnierTmp.AddTeilnehmer(ProgramMethoden.SpielerObjektErstellen("Noah", "Stroh", "Fischland", 42, ++ID), 90);
            //turnierTmp.AddTeilnehmer(ProgramMethoden.SpielerObjektErstellen("Kevin", "Schults", "Fischland", 32, ++ID), 122);
            //turnierTmp.AddTeilnehmer(ProgramMethoden.SpielerObjektErstellen("Josef", "Marx", "Fischland", 14, ++ID), 100);
            //turnierTmp.AddTeilnehmer(ProgramMethoden.SpielerObjektErstellen("Johanna", "Herbst", "Fischland", 14, ++ID), 113);

            turnierTmp = TurnierObjektErstellen("Präsidtenturnier", "03.09.2021", ++TurnierID);
            turnierTmp.AddTeilnehmer(SpielerListe[0], 100);
            turnierTmp.AddTeilnehmer(SpielerListe[1], 80);
            turnierTmp.AddTeilnehmer(SpielerListe[2], 90);
            turnierTmp.AddTeilnehmer(SpielerListe[3], 100);
            turnierTmp.AddTeilnehmer(SpielerListe[4], 120);
            TurnierListe.Add(turnierTmp);

            turnierTmp = TurnierObjektErstellen("Oktoberturnier", "03.09.2021", ++TurnierID);
            turnierTmp.AddTeilnehmer(SpielerListe[0], 200);
            turnierTmp.AddTeilnehmer(SpielerListe[1], 80);
            turnierTmp.AddTeilnehmer(SpielerListe[2], 90);
            turnierTmp.AddTeilnehmer(SpielerListe[3], 100);
            turnierTmp.AddTeilnehmer(SpielerListe[4], 120);
            TurnierListe.Add(turnierTmp);

            Menu menue = new Menu();
            while (true)
            {
                menue.MainMenu(SpielerListe, TurnierListe, ID, TurnierID);
            }

            //Funktionen

            Turnier TurnierObjektErstellen(string Turniername, string Datum, int TurnierID)
            {
                Turnier Turnier1 = new Turnier();
                Turnier1.Datum = Datum;
                Turnier1.TurnierName = Turniername;
                Turnier1.TurnierID = TurnierID;

                return Turnier1;
            }
        }
        private static Spieler SpielerObjektErstellen(string v1, string v2, string v3, int v4, int v5)
        {
            throw new NotImplementedException();
        }
    } 
}   
