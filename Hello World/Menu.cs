﻿using System;
using System.Collections.Generic;
using System.Text;
using TunierVerwaltungBusiness;

namespace Hello_World
{
    public class Menu
    {
        public void MainMenu(List<Spieler> SpielerListe, List<Turnier> TurnierListe, int ID, int TurnierID)
        {
            Console.Clear();
            Console.WriteLine("1. Neuen Spieler eingeben");
            Console.WriteLine("2. Spieler anzeigen");
            Console.WriteLine("3. Neues Turnier eingeben");
            Console.WriteLine("4. Turniere anzeigen");
            Console.WriteLine("5. Turniere und Spieler einfügen");
            Console.WriteLine("6. HDCI");
            string Auswahl = Console.ReadLine();

            if (Auswahl == "1")
            {
                Spieler SpielerAktuell = ProgramMethoden.SpielerAnlegen(ID);
                SpielerListe.Add(SpielerAktuell);
            }
            else if (Auswahl == "2")
            {
                foreach (Spieler element in SpielerListe)
                {
                    ProgramMethoden.SpielerAnzeigen(element);
                }
                Console.ReadLine();
            }
            else if (Auswahl == "3")
            {
                Turnier TurnierAktuell2 = ProgramMethoden.TurnierAnlegen(TurnierID);
                TurnierListe.Add(TurnierAktuell2);
                //foreach (Spieler element in SpielerListe)
                //{
                //    TurnierAktuell.AddTeilnehmer(element);
                //}

            }
            else if (Auswahl == "4")
            {
                foreach (Turnier element in TurnierListe)
                {
                    ProgramMethoden.TurniereAnzeigen(element);
                }
                Console.ReadLine();

            }

            else if (Auswahl == "5")
            {
                foreach (Turnier element in TurnierListe)
                {
                    ProgramMethoden.TurniereAnzeigen(element);
                }
                Console.WriteLine("TurnierID eingeben: ");
                Auswahl = Console.ReadLine();

                Turnier AusgewaehltesTurnier = new Turnier();

                //AusgewaehltesTurnier = TurnierListe.Find(i => i.TurnierID == Convert.ToInt32(Auswahl));
                
                foreach (Turnier i in TurnierListe)
                {
                    if (i.TurnierID == Convert.ToInt32(Auswahl)) AusgewaehltesTurnier = i;
                }
                
                while (true)
                {
                    Console.Clear();
                    Console.WriteLine("1. Spieler auswählen");
                    Console.WriteLine("2. Spieler anlegen");
                    Console.WriteLine("3. Fertig");
                    Auswahl = Console.ReadLine();
                    if (Auswahl == "2")
                    {
                        Spieler SpielerAktuell = ProgramMethoden.SpielerAnlegen(ID);
                        SpielerListe.Add(SpielerAktuell);
                    }
                    else if (Auswahl == "1")
                    {
                        foreach (Spieler element in SpielerListe)
                        {
                            Console.WriteLine("Spieler auswählen (ID eingeben): ");
                            ProgramMethoden.SpielerAnzeigen(element);
                        }
                        Auswahl = Console.ReadLine();
                        Console.WriteLine("Bitte die Anzahl der Schläge eingeben: ");
                        int schlaege = Convert.ToInt32(Console.ReadLine());
                        //int schlaege;
                        //schlaege = Auswahl1;

                        foreach (Spieler element in SpielerListe)
                        {
                            if (Auswahl == element.SpielerID.ToString())
                                AusgewaehltesTurnier.AddTeilnehmer(element, schlaege);
                        }
                    }
                    else if (Auswahl == "3")
                        break;
                }
            }

            else if (Auswahl == "7")
            {
                foreach (Spieler element in SpielerListe)
                {
                    ProgramMethoden.SpielerAnzeigen(element);
                }
                ID = Convert.ToInt32(Console.ReadLine());

                decimal Handicap = HDCI.Berechnung(TurnierListe, ID);             
                Console.WriteLine("Der Spieler mit der ID: " + ID + " hat " + Handicap + " Schläge im Durchschnitt in allen Turnieren gebraucht.");
                Console.ReadLine();
            }

            else if (Auswahl == "6")
            {
                List<(Spieler, decimal)> HDCIs = new List<(Spieler, decimal)>(); 
                foreach (Spieler spieler in SpielerListe)
                {
                    decimal hdci = HDCI.Berechnung(TurnierListe, spieler.SpielerID);
                    HDCIs.Add((spieler, hdci));
                    
                }
                ListSort.SortHandicapSchläge(HDCIs);
                int minNumberIterations = Math.Min(8, HDCIs.Count);
                for(int i = 0; i < minNumberIterations; i++)
                {
                    (Spieler, decimal)Rang = HDCIs[i];
                    Console.WriteLine("Der Spieler " + Rang.Item1.Vorname + " " + Rang.Item1.Nachname + " hat die Position " + (i + 1));
                    
                }

                Console.ReadLine();
            }
        }
    }
}
