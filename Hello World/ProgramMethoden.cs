﻿using System;
using System.Collections.Generic;
using System.Text;
using TunierVerwaltungBusiness;

namespace Hello_World
{
    public static class ProgramMethoden
    {
        public static void SpielerAnzeigen(Spieler Spieler1)
        {
            Console.WriteLine("Vorname: " + Spieler1.Vorname + " Nachname: " + Spieler1.Nachname + " Golf Club: " + Spieler1.Club + " HCPI: " + Spieler1.HCPI + " ID: " + Spieler1.SpielerID);
        }
        public static decimal BerechneHCPI(int hcpi)
        {
            return hcpi;
        }       
        public static void TurniereAnzeigen(Turnier Turnier1)
        {
            Turnier1.PrintYourself1();
            Console.ReadLine();
        }
        public static Spieler SpielerAnlegen(int ID)
        {
            ID++;
            Console.WriteLine("Vorname:");
            string Vorname = Console.ReadLine();
            Console.WriteLine("Nachname:");
            string Nachname = Console.ReadLine();
            Console.WriteLine(Vorname + " " + Nachname + " in welchem Club sind Sie:");
            String Club = Console.ReadLine();
            Console.WriteLine("Welchen HCPI haben sie:");
            int HCPI = Convert.ToInt32(Console.ReadLine());

            return SpielerObjektErstellen(Vorname, Nachname, Club, HCPI);
        }
        public static Turnier TurnierAnlegen(int TurnierID)
        {
            TurnierID++;
            Console.WriteLine("Turniere");
            Console.WriteLine("Name des Turniers: ");
            string Turnier = Console.ReadLine();
            Console.WriteLine("Datum vom " + Turnier + ": ");
            string Datum = Console.ReadLine();

            Turnier Turnier1 = new Turnier();
            Turnier1.TurnierName = Turnier;
            Turnier1.Datum = Datum;
            Turnier1.TurnierID = TurnierID;

            return Turnier1;
        }
        public static Spieler SpielerObjektErstellen(string vorname, string name, string club, int hcpi)
        {
            Spieler Spieler1 = new Spieler(vorname, name, club, hcpi);
            Spieler1.Vorname = vorname;
            Spieler1.Nachname = name;
            Spieler1.Club = club;
            Spieler1.HCPI = hcpi;
            

            return Spieler1;
        }       
    }
}