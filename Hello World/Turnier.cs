﻿using System;
using System.Collections.Generic;
using System.Text;
using TunierVerwaltungBusiness;

namespace Hello_World
{
    public class Turnier
    {
        public Turnier()
        {
            //Turnierteilnehmer = new List<Spieler>();
            Turnierteilnehmer = new List<(Spieler, int)>();
        }
        public String Datum { get; set; }
        public String TurnierName { get; set; }
        public int TurnierID { get; set; }
        //public List<Spieler> Turnierteilnehmer;
        public List<(Spieler, int)> Turnierteilnehmer;

        public void PrintYourself1()
        {         
            Console.WriteLine("Name des Turniers: " + TurnierName + " Datum des Turniers: " + Datum + "  ID: " + TurnierID);
            Console.WriteLine("Teilnehmer: ");
            Console.WriteLine("Schläge: ");
            foreach ((Spieler, int) SpielerSchlaegeTupel in Turnierteilnehmer)
            {
                ProgramMethoden.SpielerAnzeigen(SpielerSchlaegeTupel.Item1);
                Console.WriteLine(SpielerSchlaegeTupel.Item2.ToString());
            }
        }
        public void AddTeilnehmer(Spieler spieler, int Schlaege)
        {
            Turnierteilnehmer.Add((spieler, Schlaege));   
        }
    }
}