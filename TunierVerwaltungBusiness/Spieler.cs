﻿using System;

namespace TunierVerwaltungBusiness
{
    public class Spieler
    {
        public int SpielerID { get; set; }
        public String Vorname { get; set; }
        public String Nachname { get; set; }

        //Datentyp für HCPI rausfinden int?
        public String Club { get; set; }
        public int HCPI { get; set; }

        public Spieler(string vorname, string name, string club, int hcpi)
        {
            Vorname = vorname;
            Nachname = name;
            Club = club;
            HCPI = hcpi;
            var random = new Random();
            SpielerID = random.Next();
        }
        public override string ToString()
        {
            return SpielerID.ToString() + Vorname + Nachname + Club + HCPI;
        }
      
    }   
}
