﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TunierVerwaltungBusiness
{
    public static class ListSort
    {
        public static void SortHandicapSchläge(List<(Spieler, decimal)> Handicaps)
        {
            for(int i = 0; i < Handicaps.Count-1; i++)
            {
                for (int j = i + 1; j < Handicaps.Count; j++)
                {
                    decimal vordererWert = Handicaps[i].Item2;
                    decimal hintererWert = Handicaps[j].Item2;
                    if(vordererWert > hintererWert)
                    {
                        (Spieler, decimal) tmp = Handicaps[j];
                        Handicaps[j] = Handicaps[i];
                        Handicaps[i] = tmp;

                    }
                }
            }
        }
    }
}
