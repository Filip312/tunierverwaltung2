﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TunierVerwaltungBusiness
{
    class Turnier
    {
        public Turnier()
        {
            //Turnierteilnehmer = new List<Spieler>();
            Turnierteilnehmer = new List<(Spieler, int)>();
        }
        public String Datum { get; set; }
        public String TurnierName { get; set; }
        public int TurnierID { get; set; }
        //public List<Spieler> Turnierteilnehmer;
        public List<(Spieler, int)> Turnierteilnehmer;
        public void AddTeilnehmer(Spieler spieler, int Schlaege)
        {
            Turnierteilnehmer.Add((spieler, Schlaege));
        }
    }
}
